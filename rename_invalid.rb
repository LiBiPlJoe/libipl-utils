#!/usr/bin/ruby

# usage rename_invalid.rb DIRECTORY_NAME

DirName = ARGV[0]

if DirName == nil
  puts "usage:"
  puts ""
  puts "rename_invalid.rb DIRECTORY_NAME"
  puts ""
else

  def check_dir_contents(dirname)
    reasonable = /^[a-zA-Z0-9\ \-\._]+$/
    dirs_to_check = []
    # puts "In #{Dir.getwd}, checking directory #{dirname}..."
    Dir.chdir(dirname) {
      Dir.glob("*").each do |file_name |
        # puts "What about this? #{file_name}"

        unless reasonable.match(file_name)
          puts file_name
          new_name = ""
          file_name.each_char do | namechar |
            if reasonable.match(namechar)
              new_name << namechar
            else
              # puts "I don't like: #{namechar}"
              new_name << '_'
            end
          end
          puts "New name: #{new_name}"
          if File.exists?(new_name)
            puts "Oops, that file already exists!"
          else
            File.rename(file_name,new_name)
            file_name = new_name
          end
        end
        if Dir.exists?(file_name) && file_name != "." && file_name != ".."
          dirs_to_check << file_name
        end
      end
    }
    dirs_to_check.each do | dir_to_check|
      check_dir_contents(File.join(dirname,dir_to_check))
    end
  end

  check_dir_contents(DirName)

end